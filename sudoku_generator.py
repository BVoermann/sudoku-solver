from random import sample


def generator():
    base = 3
    side = base * base

    def pattern(r, c):
        return (base * (r % base) + r // base + c) % side

    def shuffle(s):
        return sample(s, len(s))

    rBase = range(base)
    rows = [g * base + r for g in shuffle(rBase) for r in shuffle(rBase)]
    cols = [g * base + c for g in shuffle(rBase) for c in shuffle(rBase)]
    nums = shuffle(range(1, base * base + 1))

    puzzle = [[nums[pattern(r, c)] for c in cols] for r in rows]

    return puzzle, side


def remover(puzzle, side):
    squares = side * side
    empties = squares * 3 // 4
    for p in sample(range(squares), empties):
        puzzle[p // side][p % side] = 0

    numSize = len(str(side))

    return puzzle
