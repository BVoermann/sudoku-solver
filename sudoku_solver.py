from sudoku_generator import generator, remover

new_puzzle_info = generator()
puzzle = remover(new_puzzle_info[0], new_puzzle_info[1])


def find_empty_cell(puzzle):
    for row in range(len(puzzle)):
        for col in range(len(puzzle[0])):
            if puzzle[row][col] == 0:
                return (row, col)


def solve_puzzle(puzzle):
    cell = find_empty_cell(puzzle)
    if not cell:
        return True
    else:
        row, col = cell

    for i in range(1, 10):
        if valid_cell(puzzle, i, (row, col)):
            puzzle[row][col] = i
            if solve_puzzle(puzzle):
                return True

            puzzle[row][col] = 0

    return False


def valid_cell(puzzle, num, pos):
    for row in range(len(puzzle[0])):
        if puzzle[pos[0]][row] == num and pos[1] != row:
            return False

    for col in range(len(puzzle)):
        if puzzle[col][pos[1]] == num and pos[0] != col:
            return False

    cell_x = pos[1] // 3
    cell_y = pos[0] // 3

    for i in range(cell_y * 3, cell_y * 3 + 3):
        for j in range(cell_x * 3, cell_x * 3 + 3):
            if puzzle[i][j] == num and (i, j) != pos:
                return False

    return True


def show_puzzle(puzzle):
    for i in range(len(puzzle)):
        if i % 3 == 0 and i != 0:
            print("- - - - - - - - - - - - - ")

        for j in range(len(puzzle[0])):
            if j % 3 == 0 and j != 0:
                print(" | ", end="")

            if j == 8:
                print(puzzle[i][j])
            else:
                print(str(puzzle[i][j]) + " ", end="")


show_puzzle(puzzle)
solve_puzzle(puzzle)
print("----------------")
show_puzzle(puzzle)
