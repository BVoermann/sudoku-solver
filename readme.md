<b>About the project</b>

This project aims to show my familiarity with python and is a
part of a larger effort to build a resume.


To start the project, execute the sudoku_solver.py. There no external libraries
required for this project. The python version used is: 3.10.4.


<b>The scope of the project</b>

Up until now the scope of the project is to solve every solvable 9x9 sudoku
puzzle. It takes a puzzle that gets randomly generated to make sure
it is solvable in the first place, then deletes a random amount of
numbers from it and solves it again, given only the input with
certain numbers missing.


<b>What's left to do?</b>

I could see some sort of input for the number of cells that you want to create,
so it doesn't have to be only 9x9, although that is, as far as I can tell,
the most commong form of a sudoku puzzle.

What could be a bigger undertaking is a way to fill in a puzzle yourself
without you having to change the code and write your own puzzle in
python lists. I could think of a machine learning algorithm that takes a
photograph and extracts the numbers from the sudoku.


<b>Known bugs</b>

There are no known bugs.
